import { browser, by, element, ElementFinder } from 'protractor';

export class CreatePage {
  navigateTo() {
    return browser.get('/create');
  }
  getBackLink(): ElementFinder {
    return element(by.css('.back'));
  }
  getNameFormControl(): ElementFinder {
    return element(by.css('input[name=name]'));
  }
  getCommentFormControl(): ElementFinder {
    return element(by.css('input[name=comment]'));
  }
  getImageUrlFormControl(): ElementFinder {
    return element(by.css('input[name=imageUrl]'));
  }
  getSubmitButton(): ElementFinder {
    return element(by.css('button[type=submit]'));
  }
}
