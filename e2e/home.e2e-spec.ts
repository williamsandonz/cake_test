import { HomePage } from './home.po';
import { browser } from 'protractor';
describe('HomePage', () => {
  let page: HomePage;
  beforeEach(() => {
    page = new HomePage();
  });
  it('should navigate to create cake page when the create link is clicked', async () => {
    page.navigateTo();
    const element = page.getCreateLink();
    element.click();
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/create');
  });
  it('should navigate to cake view page when a cake is clicked', async() => {
    page.navigateTo();
    const element = page.getFirstCake();
    const id = await element.getAttribute('id');
    element.click();
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/view/' + id);
  });
});
