import { browser, by, element, ElementFinder } from 'protractor';

export class HomePage {
  navigateTo() {
    return browser.get('/');
  }
  getCreateLink(): ElementFinder {
    return element(by.css('app-cake-list .add'));
  }
  getFirstCake(): ElementFinder {
    return element(by.css('app-cake-list li:first-child'));
  }
}
