import { CreatePage } from './create.po';
import { browser } from 'protractor';

describe('CreatePage', () => {
  let page: CreatePage;
  beforeEach(() => {
    page = new CreatePage();
  });
  it('should navigate to home page when back link is clicked', async () => {
    page.navigateTo();
    const element = page.getBackLink();
    element.click();
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/');
  });
  it('should navigate to home page when form is succesfully completed', async () => {
    page.navigateTo();
    page.getNameFormControl().sendKeys('a');
    page.getCommentFormControl().sendKeys('b');
    page.getImageUrlFormControl().sendKeys('c');
    page.getSubmitButton();
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/');
  });
});
