



**To install:**

    yarn

**To Develop:**

    yarn run start:client (terminal 1), yarn run start:server (terminal 2)

**To run in production mode:**

    yarn run deploy

**To run unit tests:**

    yarn run test

**To run end-to-end tests**

    yarn run e2e
    
**To run in lint:**

    yarn run lint
**Lighthouse score:**

 - Performance: 98
 - PWA: 82
 - Accessibility: 100
 - Best practices: 88
 - SEO: 100

**Things I would do with more time:**

 - https, http2, gzip (PWA benefits)
 - Livereload
 - Proxy/mock-backend solution for e2e tests
 - Move client code to /src/client
 
**Few notes:**
 
*The gulp task 'copy-ssl-certs' isn't needed as not serving over SSL because the API calls violate HTTPS security and service workers don't work with self-signed certs. Have retained it only to demonstrate some usage of gulp.*

*If debugging service worker in Chrome to test offline mode, you may need to deregister and reload the worker as I've noticed sometimes seems it doesn't work immediately.*