import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Cake } from './../../view_models/modules';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { constants } from './../../constants';
import * as _ from 'lodash';

@Component({
  selector: 'app-cake-list',
  templateUrl: './cake_list.html',
  styleUrls: ['./cake_list.scss']
})
export class CakeListComponent implements OnInit {
  cakes: Array<Cake>;
  constructor(public http: HttpClient) {
  }
  ngOnInit() {
    this.http.get(constants.apiUri + '/cakes').subscribe(
      (cakes: Array<Cake>) => {
        cakes = _.filter(cakes, (cake) => {
          return cake.name && cake.comment && cake.imageUrl;
        });
        this.cakes = cakes;
      },
      (error: HttpErrorResponse) => {
        this.onHttpError();
      }
    );
  }
  onHttpError() {
    // Example place to handle error
  }
}
