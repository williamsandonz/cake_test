import { Inject } from '@angular/core';
import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { onBeforeEach } from './../../test';
import { CakeListComponent } from './cake_list';
import { Cake } from './../../view_models/cake';
import { constants } from './../../constants';

describe('CakeListComponent', () => {
  let fixture,
    component: CakeListComponent,
    httpMock: HttpTestingController;
  beforeEach(() => {
    onBeforeEach([HttpClientTestingModule]);
    fixture = TestBed.createComponent(CakeListComponent);
    component = fixture.debugElement.componentInstance;
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    httpMock.verify();
  });
  it('should load cakes from API upon initialisation', () => {
    const cakes: Array<Cake> = [new Cake('123', 'asdf', 'fdsa', 5, 'image')];
    component.ngOnInit();
    httpMock
      .expectOne(constants.apiUri + '/cakes')
      .flush(cakes);
    expect(component.cakes).toEqual(cakes);
  });
  it('should call onHttpError() when an HTTP request fails', () => {
    const spy = jest.spyOn(component, 'onHttpError');
    component.ngOnInit();
    httpMock
      .expectOne(constants.apiUri + '/cakes')
      .flush({}, { status: 500, statusText: 'Server Error' });
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
