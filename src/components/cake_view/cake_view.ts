import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { Cake } from './../../view_models/modules';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { constants } from './../../constants';

@Component({
  selector: 'app-cake-view',
  templateUrl: './cake_view.html',
  styleUrls: ['./cake_view.scss']
})
export class CakeViewComponent implements OnInit {
  cake: Cake;
  constructor(public http: HttpClient, public route: ActivatedRoute) {

  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.getData(id);
      }
    });
  }
  getData(id: string) {
    this.http.get(constants.apiUri + '/cakes/' + id).subscribe(
      (cake: Cake) => {
        this.cake = cake;
      },
      (error: HttpErrorResponse) => {
        this.onHttpError();
      }
    );
  }
  onHttpError() {
    // Example place to handle error
  }
}

