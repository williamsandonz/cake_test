import { Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { onBeforeEach } from './../../test';
import { CakeViewComponent } from './cake_view';
import { Cake } from './../../view_models/cake';
import { constants } from './../../constants';

describe('CakeViewComponent', () => {
  let fixture,
    component: CakeViewComponent,
    httpMock: HttpTestingController;
  beforeEach(() => {
    onBeforeEach([
      HttpClientTestingModule
    ],
    [
    ],
    [
      {
        provide: ActivatedRoute,
        useValue: {
          params: Observable.of({id: 1})
        }
      }
    ]);
    fixture = TestBed.createComponent(CakeViewComponent);
    component = fixture.debugElement.componentInstance;
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    httpMock.verify();
  });
  it('should load cake from API when the route parameter "id" changes', () => {
    const cake = new Cake('123', 'asdf', 'fdsa', 5, 'image');
    component.ngOnInit();
    httpMock
      .expectOne(constants.apiUri + '/cakes/1')
      .flush(cake);
    expect(component.cake).toEqual(cake);
  });
  it('should call onHttpError() when an HTTP request fails', () => {
    const spy = jest.spyOn(component, 'onHttpError');
    component.ngOnInit();
    httpMock
      .expectOne(constants.apiUri + '/cakes/1')
      .flush({}, { status: 500, statusText: 'Server Error' });
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
