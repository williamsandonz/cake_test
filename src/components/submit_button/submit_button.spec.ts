import { FormControl } from '@angular/forms';
import { TestBed, async } from '@angular/core/testing';
import { onBeforeEach } from './../../test';
import { SubmitButtonComponent } from './submit_button';

describe('SubmitButtonComponent', () => {
  let fixture,
    component: SubmitButtonComponent;
  beforeEach(() => {
    onBeforeEach();
    fixture = TestBed.createComponent(SubmitButtonComponent);
    component = fixture.debugElement.componentInstance;
  });
  it('should mark all controls as dirty upon click', () => {
    component.controls.push(new FormControl(''));
    const control: FormControl = component.controls[0];
    component.onClick();
    expect(control.dirty).toEqual(true);
  });
});
