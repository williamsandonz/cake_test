import { Component, Input, HostBinding } from '@angular/core';
import { FormControl } from '@angular/forms';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'app-submit-button',
  templateUrl: './submit_button.html',
  styleUrls: ['./submit_button.scss']
})
export class SubmitButtonComponent {
  @Input() @HostBinding('class.processing') processing = false;
  @Input() displayBlock = false;
  @Input() disabled = false;
  @Input() type = 'primary';
  @Input() text: string;
  @Input() controls: Array<FormControl> = [];
  constructor() {
  }
  onClick() {
    if (this.controls) {
      Object.keys(this.controls).forEach((key) => {
        this.controls[key].markAsDirty();
      });
    }
  }
}
