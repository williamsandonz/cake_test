import { TestBed, async } from '@angular/core/testing';
import { onBeforeEach } from './../../test';
import { AppComponent } from './app';
describe('AppComponent', () => {
  let fixture,
  component: AppComponent;
  beforeEach(() => {
    onBeforeEach();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  });
  it('should create the app', () => {
    expect(component).toBeTruthy();
  });
});
