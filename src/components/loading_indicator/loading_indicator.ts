import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading_indicator.html',
  styleUrls: ['./loading_indicator.scss']
})
export class LoadingIndicatorComponent {
  constructor() {
  }
}
