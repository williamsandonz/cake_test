import { TestBed, async, tick, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { onBeforeEach } from './../../test';
import { CakeCreateComponent } from './cake_create';
import { Cake } from './../../view_models/modules';
import { constants } from './../../constants';

describe('CakeCreateComponent', () => {
  let fixture,
    component: CakeCreateComponent,
    httpMock: HttpTestingController;
  beforeEach(() => {
    onBeforeEach([HttpClientTestingModule, RouterTestingModule]);
    fixture = TestBed.createComponent(CakeCreateComponent);
    component = fixture.debugElement.componentInstance;
    httpMock = TestBed.get(HttpTestingController);
    component.ngOnInit();
  });
  it('should contain an invalid form upon initialisation', () => {
    expect(component.form.valid).toEqual(false);
  });
  it('should contain a property called processing set to false upon initialisation', () => {
    expect(component.processing).toEqual(false);
  });
  it('should contain and invalid form when validation criteria violated', fakeAsync(() => {
    const setFormToValidValues = () => {
      component.form.controls['name'].setValue('name');
      component.form.controls['comment'].setValue('comment');
      component.form.controls['imageUrl'].setValue('imageUrl');
      component.form.controls['yumFactor'].setValue('yumFactor');
    };
    setFormToValidValues();
    expect(component.form.valid).toEqual(true);
    component.form.controls['name'].setValue('');
    tick();
    expect(component.form.valid).toEqual(false);
    setFormToValidValues();
    component.form.controls['comment'].setValue('');
    tick();
    expect(component.form.valid).toEqual(false);
    setFormToValidValues();
    component.form.controls['imageUrl'].setValue('');
    tick();
    expect(component.form.valid).toEqual(false);
    setFormToValidValues();
    component.form.controls['yumFactor'].setValue(-999);
    tick();
    expect(component.form.valid).toEqual(false);
  }));
  it('should not post data when form is invalid', () => {
    const spy = jest.spyOn(component, 'postData');
    component.onSubmit();
    expect(spy).not.toHaveBeenCalled();
  });
  it('should post data when form is valid', () => {
    const spy = jest.spyOn(component, 'postData');
    component.form.controls['name'].setValue('name');
    component.form.controls['comment'].setValue('comment');
    component.form.controls['imageUrl'].setValue('imageUrl');
    component.onSubmit();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(component.processing).toEqual(true);
  });
  it('should post data to API and redirect to listing when successful', () => {
    const mock = jest.fn();
    component.router.navigate = mock;
    component.form.controls['name'].setValue('name');
    component.form.controls['comment'].setValue('comment');
    component.form.controls['imageUrl'].setValue('imageUrl');
    component.postData();
    const http = httpMock.expectOne(constants.apiUri + '/cakes');
    expect(http.request.body).toEqual({
      name: 'name',
      comment: 'comment',
      imageUrl: 'imageUrl',
      yumFactor: component.yumFactorMin
    });
    http.flush({});
    httpMock.verify();
    expect(component.processing).toEqual(false);
    expect(mock).toHaveBeenCalledWith(['']);
  });
});
