import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { constants } from './../../constants';
import { Response } from '@angular/http';
import { Cake } from './../../view_models/cake';

@Component({
  selector: 'app-cake-create',
  templateUrl: './cake_create.html',
  styleUrls: ['./cake_create.scss']
})
export class CakeCreateComponent implements OnInit {
  yumFactorMin = 1;
  yumFactorMax = 5;
  form: FormGroup;
  processing = false;
  constructor(public formBuilder: FormBuilder, public http: HttpClient, public router: Router) {
  }
  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.form = this.formBuilder.group({
      name: [
        '',
        Validators.required
      ],
      comment: [
        '',
        Validators.required
      ],
      imageUrl: [
        '',
        Validators.required
      ],
      yumFactor: [this.yumFactorMin,
        [
          Validators.min(this.yumFactorMin),
          Validators.max(this.yumFactorMax)
        ]
      ]
    });
  }
  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    this.postData();
  }
  postData() {
    this.processing = true;
    this.http.post(constants.apiUri + '/cakes', this.form.value).subscribe(
      (cake: Cake) => {
        this.processing = false;
        this.router.navigate(['']);
      },
      (error: HttpErrorResponse) => {
        this.processing = false;
      }
    );
  }
}
