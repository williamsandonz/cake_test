import { FormControl } from '@angular/forms';
import { TestBed, async } from '@angular/core/testing';
import { onBeforeEach } from './../../test';
import { ValidationMessageComponent } from './validation_message';

describe('ValidationMessageComponent', () => {
  let fixture,
    component: ValidationMessageComponent;
  beforeEach(() => {
    onBeforeEach();
    fixture = TestBed.createComponent(ValidationMessageComponent);
    component = fixture.debugElement.componentInstance;
  });
  it('should set cssClasses to empty when control.errors is empty and control is dirty', () => {
    const control = new FormControl();
    control.markAsDirty();
    component.control = control;
    component.ngDoCheck();
    expect(component.cssClasses).toEqual('');
  });
  it('should set cssClasses to empty when control.errors is not empty but control is pristine', () => {
    const control = new FormControl();
    control.setErrors({ required: true});
    component.control = control;
    component.ngDoCheck();
    expect(component.cssClasses).toEqual('');
  });
  it('should set cssClasses to invalid classes when control.errors is not empty and control is not pristine', () => {
    const control = new FormControl();
    control.setErrors({ required: true});
    control.markAsDirty();
    component.control = control;
    component.ngDoCheck();
    expect(component.cssClasses).toEqual('invalid-feedback is-invalid');
  });
});
