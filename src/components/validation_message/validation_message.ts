import { Component, Input, HostBinding, DoCheck } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-validation-message',
  templateUrl: './validation_message.html',
  styleUrls: ['./validation_message.scss']
})
export class ValidationMessageComponent implements DoCheck {
  @HostBinding('class') cssClasses: string;
  @Input() control: FormControl;
  messages: Array<string> = [];
  map = {
    required: 'Please enter a value'
  };
  missingMessageError = new Error('Validation map does not contain entry for key');
  ngDoCheck() {
    this.messages = [];
    const errors = Object.keys(this.control.errors || {});
    if (errors.length === 0 || this.control.pristine) {
      this.cssClasses = '';
      return;
    }
    this.cssClasses = 'invalid-feedback is-invalid';
    errors.forEach((key) => {
      if (!this.map[key]) {
        throw this.missingMessageError;
      }
      this.messages.push(this.map[key]);
    });
  }
}
