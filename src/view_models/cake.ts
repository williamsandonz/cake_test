export class Cake {
  constructor(
    public id: string,
    public name: string,
    public comment: string,
    public yumFactor: number,
    public imageUrl: string,
  ) {
  }
}
