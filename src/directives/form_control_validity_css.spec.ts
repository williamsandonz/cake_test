import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { TestBed, async } from '@angular/core/testing';
import { onBeforeEach } from './../test';
import { FormControlValidityCssDirective, FormControlState } from './form_control_validity_css';

@Component({
  template: `
    <form [formGroup]="form">
      <input
        type="text"
        class="form-control"
        name="field"
        formControlName="field"
        [appFormControlValidityCss]="form.get('field')">
    </form>`
})
class TestFormControlValidityCssComponent implements OnInit {
  form: FormGroup;
  constructor(public formBuilder: FormBuilder) {}
  ngOnInit() {
    this.form = this.formBuilder.group({
      field: [
        '',
        Validators.required
      ]}
    );
  }
}
describe('FormControlValidityCss', () => {
  let fixture,
  component: TestFormControlValidityCssComponent;
  beforeEach(() => {
    onBeforeEach([], [TestFormControlValidityCssComponent]);
    fixture = TestBed.createComponent(TestFormControlValidityCssComponent);
    component = fixture.debugElement.componentInstance;
    component.ngOnInit();
    fixture.detectChanges(); // Allow formBuilder to finish
  });
  it('should have valid css classes when control is valid', () => {
    component.form.controls['field'].setValue('value');
    fixture.detectChanges();
    const inputElement = fixture.debugElement.query(By.css('input'));
    const classList = inputElement.nativeElement.classList;
    expect(classList.contains(FormControlState.Valid)).toEqual(true);
    expect(classList.contains(FormControlState.Invalid)).toEqual(false);
  });
  it('should have css classes for invalid state when control is not pristine', () => {
    component.form.controls['field'].markAsDirty();
    fixture.detectChanges();
    const inputElement = fixture.debugElement.query(By.css('input'));
    const classList = inputElement.nativeElement.classList;
    expect(classList.contains(FormControlState.Invalid)).toEqual(true);
    expect(classList.contains(FormControlState.Valid)).toEqual(false);
  });
});
