import {Directive, ElementRef, Input, DoCheck} from '@angular/core';
import { FormControl } from '@angular/forms';

export enum FormControlState {
  Valid = 'is-valid',
  Invalid = 'is-invalid'
}

@Directive({
  selector: '[appFormControlValidityCss]'
})
export class FormControlValidityCssDirective implements DoCheck {
  public el: HTMLElement;
  @Input('appFormControlValidityCss') control: FormControl;
  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }
  ngDoCheck() {
    const classList = this.el.classList;
    if (this.control.valid) {
      classList.remove(FormControlState.Invalid);
      classList.add(FormControlState.Valid);
    } else if (!this.control.pristine) {
      classList.remove(FormControlState.Valid);
      classList.add(FormControlState.Invalid);
    }
  }
}
