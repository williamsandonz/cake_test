import * as express from 'express';
import * as path from 'path';
import * as https from 'https';
import * as fs from 'fs';

const app = express();
const clientDist = path.join(__dirname, '../', '../', 'dist', 'client');
app.use(express.static(clientDist));
app.set('views', clientDist);
app.engine('html', require('ejs').renderFile);
app.get('*', (req, res) => {
  res.render('index.html');
});

const port = 3000;

// Commented out SSL as API calls cause HTTPS violation and service workers
// don't work with self-signed certs out of the box.
// const serverDist = path.join(__dirname, '../', '../', 'dist', 'server');
// https.createServer({
//   key: fs.readFileSync(path.join(serverDist, 'ssl_certificates/key.pem')),
//   cert: fs.readFileSync(path.join(serverDist, 'ssl_certificates/certificate.crt')),
// }, app).listen(port);

app.listen(port, () => console.log('Listening on port: ' + port));
