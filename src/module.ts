import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { constants } from './constants';
import { environment } from './environments/environment';

// Components
import { AppComponent } from './components/app/app';
import { CakeCreateComponent } from './components/cake_create/cake_create';
import { CakeViewComponent } from './components/cake_view/cake_view';
import { CakeListComponent } from './components/cake_list/cake_list';
import { LoadingIndicatorComponent } from './components/loading_indicator/loading_indicator';
import { SubmitButtonComponent } from './components/submit_button/submit_button';
import { ValidationMessageComponent } from './components/validation_message/validation_message';

// Directives
import { FormControlValidityCssDirective } from './directives/form_control_validity_css';

// Providers
import { CustomErrorHandler } from './providers/error_handler';

const routes: Routes = [
  { path: '', component: CakeListComponent },
  { path: 'view/:id', component: CakeViewComponent },
  { path: 'create', component: CakeCreateComponent }
];

export const config = {
  declarations: [
    // Components
    AppComponent,
    CakeCreateComponent,
    CakeViewComponent,
    CakeListComponent,
    LoadingIndicatorComponent,
    SubmitButtonComponent,
    ValidationMessageComponent,
    // Directives
    FormControlValidityCssDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes
    ),
    ReactiveFormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: CustomErrorHandler
    }
  ],
  bootstrap: [AppComponent]
};

@NgModule(config)
export class AppModule { }

