import 'jest-preset-angular';
import {APP_BASE_HREF} from '@angular/common';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { config } from './module';

export function onBeforeEach(imports: Array<any> = [], declarations: Array<any> = [], providers: Array<any> = []) {
  TestBed.configureTestingModule({
    declarations: [config.declarations, ...declarations],
    providers: [config.providers, ...providers, ... [{provide: APP_BASE_HREF, useValue : '/' }]],
    imports: [config.imports, ...imports]
  }).compileComponents();
}
