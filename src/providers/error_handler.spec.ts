import { Component, ErrorHandler } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { onBeforeEach } from './../test';
import { CustomErrorHandler } from './error_handler';
import { environment } from './../environments/environment';

describe('ErrorHandler', () => {
  let service: CustomErrorHandler;
  const consoleErrorOriginal = console.error;
  beforeEach(() => {
    onBeforeEach();
    console.error = () => {};
    service = TestBed.get(ErrorHandler);
  });
  afterEach(() => {
    console.error = consoleErrorOriginal;
  });
  it('should report an error when the environment variable errorReporting is true', () => {
    environment.errorReporting = true;
    const spy = jest.spyOn(service, 'reportError');
    service.handleError(new Error('asdf'));
    expect(spy).toHaveBeenCalled();
  });
  it('should not report an error when the environment variable errorReporting is false', () => {
    environment.errorReporting = false;
    const spy = jest.spyOn(service, 'reportError');
    service.handleError(new Error('asdf'));
    expect(spy).not.toHaveBeenCalled();
  });
});
