import { ErrorHandler, Injectable } from '@angular/core';
import { environment } from './../environments/environment';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {
  constructor() {
  }
  handleError(error: Error) {
    console.error(error);
    if (environment.errorReporting) {
      this.reportError();
    }
  }
  reportError() {
    // Example place to log to error reporting SaaS
  }
}
