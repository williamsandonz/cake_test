const gulp = require('gulp');

gulp.task('copy-ssl-certs', function () {
  gulp.src('src/server/ssl_certificates/*', { base:'src/' })
    .pipe(gulp.dest('dist'));
});
